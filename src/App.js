import './App.css';
import {Route, Switch} from 'react-router-dom';
import Header from "./components/Header/Header";
import SimpleBottomNavigation from "./components/SimpleBottomNavigation/SimpleBottomNavigation";
import Trending from "./pages/Trending/Trending";
import Movies from "./pages/Movies/Movies";
import Series from "./pages/Series/Series";
import Info from "./pages/Info/Info";
import {useEffect, useState} from "react";
import {useDispatch} from "react-redux";
import {getMoviesGenresData, getSeriesGenresData} from "./redux/actions/entertainmentAction";
import Search from "./pages/Search/Search";
import SiteLoader from "./components/SiteLoader";

function App() {

    const [loading, setLoading] = useState(true);

    useEffect(()=>{
        setTimeout(()=>{
            setLoading(false);
        }, 100)

    },[]);

    const dispatch = useDispatch();

    useEffect(()=>{
            dispatch(getMoviesGenresData('movie'));
            dispatch(getSeriesGenresData('tv'));
    },[]);

    if (loading){
        return (
            <SiteLoader/>
        )
    }

  return (
      <>
          <Header/>
          <div className="App">

              <Switch>
                  <Route exact path='/' component={Trending}/>
                  <Route exact path='/movies' component={Movies}/>
                  <Route exact path='/series' component={Series}/>
                  <Route exact path='/search' component={Search}/>
                  <Route exact path='/info/:type/:id' component={Info}/>
              </Switch>

          </div>
          <SimpleBottomNavigation/>
      </>
  );
}

export default App;
